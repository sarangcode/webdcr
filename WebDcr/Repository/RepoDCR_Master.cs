﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDcr.EDM;

namespace WebDcr.Repository
{
    public class RepoDCR_Master
    {
        DB_WebDcrEntities db = new DB_WebDcrEntities();
        public void upd_DCR(Tbl_DCR_Master ent, HttpPostedFileBase[] filename)
        {
            string id = ent.Applicant_Id.ToString();
            var obj = db.Tbl_DCR_Master.Find(ent.Applicant_Id);
            obj.Applicant_Name = ent.Applicant_Name;
            obj.Address_Corr = ent.Address_Corr;
            obj.Mobile = ent.Mobile;
            obj.Email = ent.Email;
            obj.Address_Land = ent.Address_Land;
            obj.Applicant_Interest = ent.Applicant_Interest;
            obj.Plot_Area = ent.Plot_Area;
            obj.Authority = ent.Authority;
            obj.Permission_Id = ent.Permission_Id;
            obj.Dev_Id = ent.Dev_Id;
            obj.SpecialControl_Id = ent.SpecialControl_Id;
            obj.Land_Description = ent.Land_Description;
            obj.District_Id = ent.District_Id;
            obj.Taluka_Id = ent.Taluka_Id;
            obj.Village_Id = ent.Village_Id;
            obj.Tps_NameNumber = ent.Tps_NameNumber;
            obj.RevenueVillage_No = ent.RevenueVillage_No;
            obj.GamtalCS_No = ent.GamtalCS_No;
            obj.Sector_No = ent.Sector_No;
            obj.OP_No = ent.OP_No;
            obj.FP_No = ent.FP_No;
            obj.Subplot_No = ent.Subplot_No;
            obj.Tenament_No = ent.Tenament_No;
            obj.City_Servey_Ward_No = ent.City_Servey_Ward_No;
            obj.Abutting_Road_Width = ent.Abutting_Road_Width;
            obj.BuildType_Id = ent.BuildType_Id;
            obj.Zone_Id = ent.Zone_Id;
            obj.IsPlot_Approved = ent.IsPlot_Approved;
            obj.DetailsofApproval = ent.DetailsofApproval;
            obj.TypeCase_Id = ent.TypeCase_Id;
            obj.Mean_Sea_Level = ent.Mean_Sea_Level;
            obj.Drc_No = ent.Drc_No;
            obj.Drc_Owner = ent.Drc_Owner;
            obj.Drc_Area = ent.Drc_Area;
            obj.Aerodrome_Type = ent.Aerodrome_Type;
            obj.AeroDistance = ent.AeroDistance;
            obj.AeroDistanceFunnel = ent.AeroDistanceFunnel;
            obj.AirportPermises = ent.AirportPermises;
            obj.ObtainedPermission = ent.ObtainedPermission;
            obj.Existing_Vacant = ent.Existing_Vacant;
            obj.Existing_PartyBuilt = ent.Existing_PartyBuilt;
            obj.Existing_FullyBuilt = ent.Existing_FullyBuilt;
            obj.Proposed_Vacant = ent.Proposed_Vacant;
            obj.Proposed_PartlyBuilt = ent.Proposed_PartlyBuilt;
            obj.Proposed_FullyBuilt = ent.Proposed_FullyBuilt;
            obj.East = ent.East;
            obj.East_OtherDetail = ent.East_OtherDetail;
            obj.East_SubDetail = ent.East_SubDetail;
            obj.East_WidthOfRoad = ent.East_WidthOfRoad;
            obj.West = ent.West;
            obj.West_OtherDetail = ent.West_OtherDetail;
            obj.West_SubDetail = ent.West_SubDetail;
            obj.West_WidthOfRoad = ent.West_WidthOfRoad;
            obj.North = ent.North;
            obj.North_OtherDetail = ent.North_OtherDetail;
            obj.North_SubDetail = ent.North_SubDetail;
            obj.North_WidthOfRoad = ent.North_WidthOfRoad;
            obj.South = ent.South;
            obj.South_OtherDetail = ent.South_OtherDetail;
            obj.South_SubDetail = ent.South_SubDetail;
            obj.South_WidthOfRoad = ent.South_WidthOfRoad;
            obj.Seismic_Zone = ent.Seismic_Zone;
            obj.WaterSupply = ent.WaterSupply;
            obj.Water_Details = ent.Water_Details;
            obj.DrainageFacility = ent.DrainageFacility;
            obj.Drainage_Details = ent.Drainage_Details;
            obj.City_Network = ent.City_Network;
            obj.Percolation_Pit = ent.Percolation_Pit;
            obj.Percolating_Well = ent.Percolating_Well;
            obj.Recharge_Pit = ent.Recharge_Pit;
            obj.SolidWaste = ent.SolidWaste;
            obj.SolidWaste_Details = ent.SolidWaste_Details;
            obj.ElectricFacility = ent.ElectricFacility;
            obj.ElectricFacility_Details = ent.ElectricFacility_Details;
            obj.Special_Road = ent.Special_Road;
            obj.Common_Basement = ent.Common_Basement;
            obj.LandUse_Detail = ent.LandUse_Detail;
            obj.LandSubUse_Detail = ent.LandSubUse_Detail;
            obj.SubType_Detail = ent.SubType_Detail;
            obj.Total_Unit = ent.Total_Unit;
            obj.Max_No_Floor = ent.Max_No_Floor;
            obj.Max_Building_Height = ent.Pro_Max_Building_Height;
            obj.Total_BuiltUp = ent.Total_BuiltUp;
            obj.Pro_LandUse_Detail = ent.Pro_LandUse_Detail;
            obj.Pro_LandSubUse_Detail = ent.Pro_LandSubUse_Detail;
            obj.Pro_SubType_Detail = ent.Pro_SubType_Detail;
            obj.Pro_Total_Unit = ent.Pro_Total_Unit;
            obj.Pro_Max_No_Floor = ent.Pro_Max_No_Floor;
            obj.Pro_Max_Building_Height = ent.Pro_Max_Building_Height;
            obj.Pro_Total_BuiltUp = ent.Pro_Total_BuiltUp;
            obj.Arc_Lic_No = ent.Arc_Lic_No;
            obj.Arc_Lic_ExpDate = ent.Arc_Lic_ExpDate;
            obj.Arc_Name = ent.Arc_Name;
            obj.Stru_Lic_No = ent.Stru_Lic_No;
            obj.Stru_Lic_ExpDate = ent.Stru_Lic_ExpDate;
            obj.Stru_Name = ent.Stru_Name;
            obj.Clerk_Lic_No = ent.Clerk_Lic_No;
            obj.Clerk_Lic_ExpDate = ent.Clerk_Lic_ExpDate;
            obj.Clerk_Name = ent.Clerk_Name;
            obj.ResonForUpdate = ent.ResonForUpdate;
            obj.Tbl_Document.Name = ent.Tbl_Document.Name;
            obj.Tbl_Document.Place = ent.Tbl_Document.Place;
            obj.Tbl_Document.Survey_No = ent.Tbl_Document.Survey_No;
            obj.Tbl_Document.Taluka = ent.Tbl_Document.Taluka;
            obj.Tbl_Document.Village = ent.Tbl_Document.Village;
            obj.Tbl_Document.District = ent.Tbl_Document.District;
            obj.Tbl_Document.Date = ent.Tbl_Document.Date;
            obj.Tbl_Document.isAgree = ent.Tbl_Document.isAgree;

            
                if(filename[0] != null)
                {
                    obj.Tbl_Document.Doc_Noc = manupulateFile(filename[0], id, "Doc_Noc");
                }
                if (filename[1] != null)
                {
                    obj.Tbl_Document.Doc_Record = manupulateFile(filename[1], id, "Doc_Record"); 
                }
                if (filename[2] != null)
                {
                    obj.Tbl_Document.Doc_Affidavit = manupulateFile(filename[2], id, "Doc_Affidavit");
                }
                if (filename[3] != null)
                {
                    obj.Tbl_Document.Doc_Power = manupulateFile(filename[3], id, "Doc_Power");
                }
                if (filename[4] != null)
                {
                    obj.Tbl_Document.Doc_Notarized = manupulateFile(filename[4], id, "Doc_Notarized");
                }
                if (filename[5] != null)
                {
                    obj.Tbl_Document.Doc_Revenue = manupulateFile(filename[5], id, "Doc_Revenue");
                }
                if (filename[6] != null)
                {
                    obj.Tbl_Document.Doc_DILR = manupulateFile(filename[6], id, "Doc_DILR");
                }
                if (filename[7] != null)
                {
                    obj.Tbl_Document.Doc_Layout = manupulateFile(filename[7], id, "Doc_Layout");
                }
                if (filename[8] != null)
                {
                    obj.Tbl_Document.Doc_TPDP = manupulateFile(filename[8], id, "Doc_TPDP");
                }
                if (filename[9] != null)
                {
                    obj.Tbl_Document.Doc_Google = manupulateFile(filename[9], id, "Doc_Google");
                }
                if (filename[10] != null)
                {
                    obj.Tbl_Document.Doc_Sanctioned = manupulateFile(filename[10], id, "Doc_Sanctioned");
                }
                if (filename[11] != null)
                {
                    obj.Tbl_Document.Doc_Upload = manupulateFile(filename[11], id, "Doc_Upload");
                }
                if (filename[12] != null)
                {
                    obj.Tbl_Document.Doc_Repairs = manupulateFile(filename[12], id, "Doc_Repairs");
                }
                if (filename[13] != null)
                {
                    obj.Tbl_Document.Doc_application = manupulateFile(filename[13], id, "Doc_application");
                }
                if (filename[14] != null)
                {
                    obj.Tbl_Document.Doc_Encolse = manupulateFile(filename[14], id, "Doc_Encolse");
                }
                if (filename[15] != null)
                {
                    obj.Tbl_Document.Doc_Coordinates = manupulateFile(filename[15], id, "Doc_Coordinates");
                }
                if (filename[16] != null)
                {
                    obj.Tbl_Document.Doc_Elevation = manupulateFile(filename[16], id, "Doc_Elevation");
                }
                
            db.SaveChanges();
        }

        public void add_DocDCR(Tbl_DCR_Master ent,HttpPostedFileBase[] filename)
        {
            string id = ent.Applicant_Id.ToString();
            if (filename[0] != null)
            {
                ent.Tbl_Document.Doc_Noc = manupulateFile(filename[0], id, "Doc_Noc");
            }
            if (filename[1] != null)
            {
                ent.Tbl_Document.Doc_Record = manupulateFile(filename[1], id, "Doc_Record");
            }
            if (filename[2] != null)
            {
                ent.Tbl_Document.Doc_Affidavit = manupulateFile(filename[2], id, "Doc_Affidavit");
            }
            if (filename[3] != null)
            {
                ent.Tbl_Document.Doc_Power = manupulateFile(filename[3], id, "Doc_Power");
            }
            if (filename[4] != null)
            {
                ent.Tbl_Document.Doc_Notarized = manupulateFile(filename[4], id, "Doc_Notarized");
            }
            if (filename[5] != null)
            {
                ent.Tbl_Document.Doc_Revenue = manupulateFile(filename[5], id, "Doc_Revenue");
            }
            if (filename[6] != null)
            {
                ent.Tbl_Document.Doc_DILR = manupulateFile(filename[6], id, "Doc_DILR");
            }
            if (filename[7] != null)
            {
                ent.Tbl_Document.Doc_Layout = manupulateFile(filename[7], id, "Doc_Layout");
            }
            if (filename[8] != null)
            {
                ent.Tbl_Document.Doc_TPDP = manupulateFile(filename[8], id, "Doc_TPDP");
            }
            if (filename[9] != null)
            {
                ent.Tbl_Document.Doc_Google = manupulateFile(filename[9], id, "Doc_Google");
            }
            if (filename[10] != null)
            {
                ent.Tbl_Document.Doc_Sanctioned = manupulateFile(filename[10], id, "Doc_Sanctioned");
            }
            if (filename[11] != null)
            {
                ent.Tbl_Document.Doc_Upload = manupulateFile(filename[11], id, "Doc_Upload");
            }
            if (filename[12] != null)
            {
                ent.Tbl_Document.Doc_Repairs = manupulateFile(filename[12], id, "Doc_Repairs");
            }
            if (filename[13] != null)
            {
                ent.Tbl_Document.Doc_application = manupulateFile(filename[13], id, "Doc_application");
            }
            if (filename[14] != null)
            {
                ent.Tbl_Document.Doc_Encolse = manupulateFile(filename[14], id, "Doc_Encolse");
            }
            if (filename[15] != null)
            {
                ent.Tbl_Document.Doc_Coordinates = manupulateFile(filename[15], id, "Doc_Coordinates");
            }
            if (filename[16] != null)
            {
                ent.Tbl_Document.Doc_Elevation = manupulateFile(filename[16], id, "Doc_Elevation");
            }

            db.Tbl_DCR_Master.Add(ent);
            db.SaveChanges();

        }

        public string manupulateFile(HttpPostedFileBase file, string id, string name)
        {
            string pic = System.IO.Path.GetFileName(file.FileName);
            string extension = System.IO.Path.GetExtension(file.FileName);
            string path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Content/files/"), (id + name) + extension);
            // file is uploaded
            file.SaveAs(path);
            string fullpath = "/Content/files/" + ((id + name) + extension);
            return fullpath;
            
        }
    }
}