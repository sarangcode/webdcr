//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebDcr.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AerodromeDetail_List
    {
        public int Applicant_Id { get; set; }
        public string Aerodrome_Type { get; set; }
        public string AeroDistance { get; set; }
        public string AeroDistanceFunnel { get; set; }
    
        public virtual Tbl_OwnerShip_Details Tbl_OwnerShip_Details { get; set; }
    }
}
