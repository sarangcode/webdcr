//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebDcr.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_BuildingType_List
    {
        public Tbl_BuildingType_List()
        {
            this.Tbl_DCR_Master = new HashSet<Tbl_DCR_Master>();
            this.Tbl_LandDetails = new HashSet<Tbl_LandDetails>();
        }
    
        public int BuildType_Id { get; set; }
        public string BuildingType { get; set; }
    
        public virtual ICollection<Tbl_DCR_Master> Tbl_DCR_Master { get; set; }
        public virtual ICollection<Tbl_LandDetails> Tbl_LandDetails { get; set; }
    }
}
