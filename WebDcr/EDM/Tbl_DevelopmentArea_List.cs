//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebDcr.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_DevelopmentArea_List
    {
        public Tbl_DevelopmentArea_List()
        {
            this.Tbl_DCR_Master = new HashSet<Tbl_DCR_Master>();
            this.Tbl_Development_Para = new HashSet<Tbl_Development_Para>();
        }
    
        public int Dev_Id { get; set; }
        public string DevelopmentArea { get; set; }
    
        public virtual ICollection<Tbl_DCR_Master> Tbl_DCR_Master { get; set; }
        public virtual ICollection<Tbl_Development_Para> Tbl_Development_Para { get; set; }
    }
}
