﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebDcr.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Create", "Registration");
        }
    }
}