﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebDcr.EDM;
using WebDcr.Repository;

namespace WebDcr.Controllers
{
    public class DCR_MasterController : Controller
    {
        private DB_WebDcrEntities db = new DB_WebDcrEntities();

        // GET: DCR_Master
        public ActionResult Index()
        {
            var tbl_DCR_Master = db.Tbl_DCR_Master.Include(t => t.Tbl_BodyRegion).Include(t => t.Tbl_BuildingType_List).Include(t => t.Tbl_DevelopmentArea_List).Include(t => t.Tbl_District_List).Include(t => t.Tbl_DrainageFacility_List).Include(t => t.Tbl_ElectricityFacility_List).Include(t => t.Tbl_LandSubUseDetail_List).Include(t => t.Tbl_LandSubUseDetail_List1).Include(t => t.Tbl_LandUseDetail_List).Include(t => t.Tbl_LandUseDetail_List1).Include(t => t.Tbl_PermissionType_List).Include(t => t.Tbl_Site_List).Include(t => t.Tbl_Site_List1).Include(t => t.Tbl_Site_List2).Include(t => t.Tbl_Site_List3).Include(t => t.Tbl_SolidWasteDisposalFacility_List).Include(t => t.Tbl_SpecialControlArea_List).Include(t => t.Tbl_SpecialRoad_List).Include(t => t.Tbl_Taluka_List).Include(t => t.Tbl_TypeCase_List).Include(t => t.Tbl_Village_List).Include(t => t.Tbl_WaterSupplyFacility_List).Include(t => t.Tbl_Zone_List).Include(t => t.Tbl_Document);
            return View(tbl_DCR_Master.ToList());
        }

        // GET: DCR_Master/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_DCR_Master tbl_DCR_Master = db.Tbl_DCR_Master.Find(id);
            if (tbl_DCR_Master == null)
            {
                return HttpNotFound();
            }
            return View(tbl_DCR_Master);
        }

        // GET: DCR_Master/Create
        public ActionResult Create()
        {
            int applicant_Id = Convert.ToInt32(Session["UserID"].ToString());
            var obj = db.Tbl_DCR_Master.Where(a => a.Applicant_Id.Equals(applicant_Id)).FirstOrDefault();

            ViewBag.Authority = new SelectList(db.Tbl_BodyRegion, "Id_BodyRegion", "BodyRegion");
            ViewBag.BuildType_Id = new SelectList(db.Tbl_BuildingType_List, "BuildType_Id", "BuildingType");
            ViewBag.Dev_Id = new SelectList(db.Tbl_DevelopmentArea_List, "Dev_Id", "DevelopmentArea");
            ViewBag.District_Id = new SelectList(db.Tbl_District_List, "District_Id", "District");
            ViewBag.DrainageFacility = new SelectList(db.Tbl_DrainageFacility_List, "DrainageFacility_Id", "DrainageFacility");
            ViewBag.ElectricFacility = new SelectList(db.Tbl_ElectricityFacility_List, "ElectricityFacility_Id", "Electricity_Facility");
            ViewBag.LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail");
            ViewBag.Pro_LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail");
            ViewBag.LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail");
            ViewBag.Pro_LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail");
            ViewBag.Permission_Id = new SelectList(db.Tbl_PermissionType_List, "Permission_Id", "Permission_Type");
            ViewBag.East = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail");
            ViewBag.West = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail");
            ViewBag.North = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail");
            ViewBag.South = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail");
            ViewBag.SolidWaste = new SelectList(db.Tbl_SolidWasteDisposalFacility_List, "SolidWaste_Id", "Solid_Waste");
            ViewBag.SpecialControl_Id = new SelectList(db.Tbl_SpecialControlArea_List, "SpecialControl_Id", "SpeacialControl_Area");
            ViewBag.Special_Road = new SelectList(db.Tbl_SpecialRoad_List, "SpecialRoad_Id", "Special_Road");
            ViewBag.Taluka_Id = new SelectList(db.Tbl_Taluka_List.Where(x => x.District_Id == 1), "Taluka_Id", "Taluka");
            ViewBag.TypeCase_Id = new SelectList(db.Tbl_TypeCase_List, "TypeCase_Id", "Type_Case");
            ViewBag.Village_Id = new SelectList(db.Tbl_Village_List.Where(x => x.Taluka_Id == 1), "Village_Id", "Village");
            ViewBag.WaterSupply = new SelectList(db.Tbl_WaterSupplyFacility_List, "Water_Id", "WaterSupply");
            ViewBag.Zone_Id = new SelectList(db.Tbl_Zone_List, "Zone_Id", "Zone");
            ViewBag.Applicant_Id = new SelectList(db.Tbl_Document, "Applicant_Id", "Doc_Noc");

            if (obj == null)
            {
                return View();
            }
            else
            {

                Tbl_DCR_Master tbl_DCR_Master = db.Tbl_DCR_Master.Find(applicant_Id);
               
                //tbl_DCR_Master.Existing_Vacant = 
                return View(tbl_DCR_Master);
            }
            
        }

        // POST: DCR_Master/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tbl_DCR_Master tbl_DCR_Master, HttpPostedFileBase[] filename)
        {
            int applicant_Id = Convert.ToInt32(Session["UserID"].ToString());
            var obj = db.Tbl_DCR_Master.Where(a => a.Applicant_Id.Equals(applicant_Id)).FirstOrDefault();

            RepoDCR_Master objRepo = new RepoDCR_Master();
            if(obj == null)
            {
                tbl_DCR_Master.Applicant_Id = applicant_Id;
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    //db.Tbl_DCR_Master.Add(tbl_DCR_Master);
                    //db.SaveChanges();
                    objRepo.add_DocDCR(tbl_DCR_Master, filename);
                    return RedirectToAction("create");
                }
            }
            else
            {
                if(ModelState.IsValid)
                { 
                tbl_DCR_Master.Applicant_Id = applicant_Id;
                objRepo.upd_DCR(tbl_DCR_Master, filename);
                return RedirectToAction("create");
                //upd_DCR(tbl_DCR_Master);
                }
               
            }
           

            ViewBag.Authority = new SelectList(db.Tbl_BodyRegion, "Id_BodyRegion", "BodyRegion", tbl_DCR_Master.Authority);
            ViewBag.BuildType_Id = new SelectList(db.Tbl_BuildingType_List, "BuildType_Id", "BuildingType", tbl_DCR_Master.BuildType_Id);
            ViewBag.Dev_Id = new SelectList(db.Tbl_DevelopmentArea_List, "Dev_Id", "DevelopmentArea", tbl_DCR_Master.Dev_Id);
            ViewBag.District_Id = new SelectList(db.Tbl_District_List, "District_Id", "District", tbl_DCR_Master.District_Id);
            ViewBag.DrainageFacility = new SelectList(db.Tbl_DrainageFacility_List, "DrainageFacility_Id", "DrainageFacility", tbl_DCR_Master.DrainageFacility);
            ViewBag.ElectricFacility = new SelectList(db.Tbl_ElectricityFacility_List, "ElectricityFacility_Id", "Electricity_Facility", tbl_DCR_Master.ElectricFacility);
            ViewBag.LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.LandSubUse_Detail);
            ViewBag.Pro_LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.Pro_LandSubUse_Detail);
            ViewBag.LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.LandUse_Detail);
            ViewBag.Pro_LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.Pro_LandUse_Detail);
            ViewBag.Permission_Id = new SelectList(db.Tbl_PermissionType_List, "Permission_Id", "Permission_Type", tbl_DCR_Master.Permission_Id);
            ViewBag.East = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.East);
            ViewBag.West = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.West);
            ViewBag.North = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.North);
            ViewBag.South = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.South);
            ViewBag.SolidWaste = new SelectList(db.Tbl_SolidWasteDisposalFacility_List, "SolidWaste_Id", "Solid_Waste", tbl_DCR_Master.SolidWaste);
            ViewBag.SpecialControl_Id = new SelectList(db.Tbl_SpecialControlArea_List, "SpecialControl_Id", "SpeacialControl_Area", tbl_DCR_Master.SpecialControl_Id);
            ViewBag.Special_Road = new SelectList(db.Tbl_SpecialRoad_List, "SpecialRoad_Id", "Special_Road", tbl_DCR_Master.Special_Road);
            ViewBag.Taluka_Id = new SelectList(db.Tbl_Taluka_List, "Taluka_Id", "Taluka", tbl_DCR_Master.Taluka_Id);
            ViewBag.TypeCase_Id = new SelectList(db.Tbl_TypeCase_List, "TypeCase_Id", "Type_Case", tbl_DCR_Master.TypeCase_Id);
            ViewBag.Village_Id = new SelectList(db.Tbl_Village_List, "Village_Id", "Village", tbl_DCR_Master.Village_Id);
            ViewBag.WaterSupply = new SelectList(db.Tbl_WaterSupplyFacility_List, "Water_Id", "WaterSupply", tbl_DCR_Master.WaterSupply);
            ViewBag.Zone_Id = new SelectList(db.Tbl_Zone_List, "Zone_Id", "Zone", tbl_DCR_Master.Zone_Id);
            ViewBag.Applicant_Id = new SelectList(db.Tbl_Document, "Applicant_Id", "Doc_Noc", tbl_DCR_Master.Applicant_Id);
            return View(tbl_DCR_Master);
        }

        // GET: DCR_Master/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_DCR_Master tbl_DCR_Master = db.Tbl_DCR_Master.Find(id);
            if (tbl_DCR_Master == null)
            {
                return HttpNotFound();
            }
            ViewBag.Authority = new SelectList(db.Tbl_BodyRegion, "Id_BodyRegion", "BodyRegion", tbl_DCR_Master.Authority);
            ViewBag.BuildType_Id = new SelectList(db.Tbl_BuildingType_List, "BuildType_Id", "BuildingType", tbl_DCR_Master.BuildType_Id);
            ViewBag.Dev_Id = new SelectList(db.Tbl_DevelopmentArea_List, "Dev_Id", "DevelopmentArea", tbl_DCR_Master.Dev_Id);
            ViewBag.District_Id = new SelectList(db.Tbl_District_List, "District_Id", "District", tbl_DCR_Master.District_Id);
            ViewBag.DrainageFacility = new SelectList(db.Tbl_DrainageFacility_List, "DrainageFacility_Id", "DrainageFacility", tbl_DCR_Master.DrainageFacility);
            ViewBag.ElectricFacility = new SelectList(db.Tbl_ElectricityFacility_List, "ElectricityFacility_Id", "Electricity_Facility", tbl_DCR_Master.ElectricFacility);
            ViewBag.LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.LandSubUse_Detail);
            ViewBag.Pro_LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.Pro_LandSubUse_Detail);
            ViewBag.LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.LandUse_Detail);
            ViewBag.Pro_LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.Pro_LandUse_Detail);
            ViewBag.Permission_Id = new SelectList(db.Tbl_PermissionType_List, "Permission_Id", "Permission_Type", tbl_DCR_Master.Permission_Id);
            ViewBag.East = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.East);
            ViewBag.West = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.West);
            ViewBag.North = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.North);
            ViewBag.South = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.South);
            ViewBag.SolidWaste = new SelectList(db.Tbl_SolidWasteDisposalFacility_List, "SolidWaste_Id", "Solid_Waste", tbl_DCR_Master.SolidWaste);
            ViewBag.SpecialControl_Id = new SelectList(db.Tbl_SpecialControlArea_List, "SpecialControl_Id", "SpeacialControl_Area", tbl_DCR_Master.SpecialControl_Id);
            ViewBag.Special_Road = new SelectList(db.Tbl_SpecialRoad_List, "SpecialRoad_Id", "Special_Road", tbl_DCR_Master.Special_Road);
            ViewBag.Taluka_Id = new SelectList(db.Tbl_Taluka_List, "Taluka_Id", "Taluka", tbl_DCR_Master.Taluka_Id);
            ViewBag.TypeCase_Id = new SelectList(db.Tbl_TypeCase_List, "TypeCase_Id", "Type_Case", tbl_DCR_Master.TypeCase_Id);
            ViewBag.Village_Id = new SelectList(db.Tbl_Village_List, "Village_Id", "Village", tbl_DCR_Master.Village_Id);
            ViewBag.WaterSupply = new SelectList(db.Tbl_WaterSupplyFacility_List, "Water_Id", "WaterSupply", tbl_DCR_Master.WaterSupply);
            ViewBag.Zone_Id = new SelectList(db.Tbl_Zone_List, "Zone_Id", "Zone", tbl_DCR_Master.Zone_Id);
            ViewBag.Applicant_Id = new SelectList(db.Tbl_Document, "Applicant_Id", "Doc_Noc", tbl_DCR_Master.Applicant_Id);
            return View(tbl_DCR_Master);
        }

        // POST: DCR_Master/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Applicant_Id,Applicant_Name,Address_Corr,Mobile,Email,Address_Land,Applicant_Interest,Plot_Area,Authority,Permission_Id,Dev_Id,SpecialControl_Id,Land_Description,District_Id,Taluka_Id,Village_Id,Tps_NameNumber,RevenueVillage_No,GamtalCS_No,Sector_No,OP_No,FP_No,Subplot_No,Tenament_No,City_Servey_Ward_No,Abutting_Road_Width,BuildType_Id,Zone_Id,IsPlot_Approved,DetailsofApproval,TypeCase_Id,Mean_Sea_Level,Drc_No,Drc_Owner,Drc_Area,Aerodrome_Type,AeroDistance,AeroDistanceFunnel,AirportPermises,ObtainedPermission,Existing,Proposed,East,East_OtherDetail,East_SubDetail,East_WidthOfRoad,West,West_OtherDetail,West_SubDetail,West_WidthOfRoad,North,North_OtherDetail,North_SubDetail,North_WidthOfRoad,South,South_OtherDetail,South_SubDetail,South_WidthOfRoad,Seismic_Zone,WaterSupply,Water_Details,DrainageFacility,Drainage_Details,City_Network,Percolation_Pit,Percolating_Well,Recharge_Pit,SolidWaste,SolidWaste_Details,ElectricFacility,ElectricFacility_Details,Special_Road,Common_Basement,LandUse_Detail,LandSubUse_Detail,SubType_Detail,Total_Unit,Max_No_Floor,Max_Building_Height,Total_BuiltUp,Pro_LandUse_Detail,Pro_LandSubUse_Detail,Pro_SubType_Detail,Pro_Total_Unit,Pro_Max_No_Floor,Pro_Max_Building_Height,Pro_Total_BuiltUp,Arc_Lic_No,Arc_Lic_ExpDate,Arc_Name,Stru_Lic_No,Stru_Lic_ExpDate,Stru_Name,Clerk_Lic_No,Clerk_Lic_ExpDate,Clerk_Name,ResonForUpdate")] Tbl_DCR_Master tbl_DCR_Master)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_DCR_Master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Authority = new SelectList(db.Tbl_BodyRegion, "Id_BodyRegion", "BodyRegion", tbl_DCR_Master.Authority);
            ViewBag.BuildType_Id = new SelectList(db.Tbl_BuildingType_List, "BuildType_Id", "BuildingType", tbl_DCR_Master.BuildType_Id);
            ViewBag.Dev_Id = new SelectList(db.Tbl_DevelopmentArea_List, "Dev_Id", "DevelopmentArea", tbl_DCR_Master.Dev_Id);
            ViewBag.District_Id = new SelectList(db.Tbl_District_List, "District_Id", "District", tbl_DCR_Master.District_Id);
            ViewBag.DrainageFacility = new SelectList(db.Tbl_DrainageFacility_List, "DrainageFacility_Id", "DrainageFacility", tbl_DCR_Master.DrainageFacility);
            ViewBag.ElectricFacility = new SelectList(db.Tbl_ElectricityFacility_List, "ElectricityFacility_Id", "Electricity_Facility", tbl_DCR_Master.ElectricFacility);
            ViewBag.LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.LandSubUse_Detail);
            ViewBag.Pro_LandSubUse_Detail = new SelectList(db.Tbl_LandSubUseDetail_List, "LandSub_Id", "LandSub_Detail", tbl_DCR_Master.Pro_LandSubUse_Detail);
            ViewBag.LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.LandUse_Detail);
            ViewBag.Pro_LandUse_Detail = new SelectList(db.Tbl_LandUseDetail_List, "LandDetail_Id", "Land_Detail", tbl_DCR_Master.Pro_LandUse_Detail);
            ViewBag.Permission_Id = new SelectList(db.Tbl_PermissionType_List, "Permission_Id", "Permission_Type", tbl_DCR_Master.Permission_Id);
            ViewBag.East = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.East);
            ViewBag.West = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.West);
            ViewBag.North = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.North);
            ViewBag.South = new SelectList(db.Tbl_Site_List, "Site_Id", "Site_Detail", tbl_DCR_Master.South);
            ViewBag.SolidWaste = new SelectList(db.Tbl_SolidWasteDisposalFacility_List, "SolidWaste_Id", "Solid_Waste", tbl_DCR_Master.SolidWaste);
            ViewBag.SpecialControl_Id = new SelectList(db.Tbl_SpecialControlArea_List, "SpecialControl_Id", "SpeacialControl_Area", tbl_DCR_Master.SpecialControl_Id);
            ViewBag.Special_Road = new SelectList(db.Tbl_SpecialRoad_List, "SpecialRoad_Id", "Special_Road", tbl_DCR_Master.Special_Road);
            ViewBag.Taluka_Id = new SelectList(db.Tbl_Taluka_List, "Taluka_Id", "Taluka", tbl_DCR_Master.Taluka_Id);
            ViewBag.TypeCase_Id = new SelectList(db.Tbl_TypeCase_List, "TypeCase_Id", "Type_Case", tbl_DCR_Master.TypeCase_Id);
            ViewBag.Village_Id = new SelectList(db.Tbl_Village_List, "Village_Id", "Village", tbl_DCR_Master.Village_Id);
            ViewBag.WaterSupply = new SelectList(db.Tbl_WaterSupplyFacility_List, "Water_Id", "WaterSupply", tbl_DCR_Master.WaterSupply);
            ViewBag.Zone_Id = new SelectList(db.Tbl_Zone_List, "Zone_Id", "Zone", tbl_DCR_Master.Zone_Id);
            ViewBag.Applicant_Id = new SelectList(db.Tbl_Document, "Applicant_Id", "Doc_Noc", tbl_DCR_Master.Applicant_Id);
            return View(tbl_DCR_Master);
        }

        // GET: DCR_Master/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_DCR_Master tbl_DCR_Master = db.Tbl_DCR_Master.Find(id);
            if (tbl_DCR_Master == null)
            {
                return HttpNotFound();
            }
            return View(tbl_DCR_Master);
        }

        // POST: DCR_Master/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_DCR_Master tbl_DCR_Master = db.Tbl_DCR_Master.Find(id);
            db.Tbl_DCR_Master.Remove(tbl_DCR_Master);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetTaluka(int id)
        {
            var taluka_l = db.Tbl_Taluka_List.Where(x => x.Tbl_District_List.District_Id == id).ToList();
            List<SelectListItem> Taluka = new List<SelectListItem>();

            foreach (var item in taluka_l)
            {
                Taluka.Add(new SelectListItem { Text = item.Taluka, Value = item.Taluka_Id.ToString() });
            }
            return Json(new SelectList(Taluka, "Value", "Text"));
        }

        public JsonResult GetVillage(int id)
        {
            var village_l = db.Tbl_Village_List.Where(x => x.Tbl_Taluka_List.Taluka_Id == id).ToList();
            List<SelectListItem> Village = new List<SelectListItem>();

            foreach (var item in village_l)
            {
                Village.Add(new SelectListItem { Text = item.Village, Value = item.Village_Id.ToString() });
            }
            return Json(new SelectList(Village, "Value", "Text"));
        }
    }
}
