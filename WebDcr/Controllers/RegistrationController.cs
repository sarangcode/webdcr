﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebDcr.EDM;

namespace WebDcr.Controllers
{
    public class RegistrationController : Controller
    {
        private DB_WebDcrEntities db = new DB_WebDcrEntities();

        // GET: Registration
        public ActionResult Index()
        {
            return View(db.Tbl_Registration.ToList());
        }

        // GET: Registration/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Registration tbl_Registration = db.Tbl_Registration.Find(id);
            if (tbl_Registration == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Registration);
        }

        // GET: Registration/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Registration/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "User_Id,First_Name,Last_Name,Pan_Number,Company_Name,Mobile,Email,Password,Re_Password")] Tbl_Registration tbl_Registration)
        {
            if(tbl_Registration.User_Id == 0)
            {
                tbl_Registration.User_Id = generateID();
            }
            if (ModelState.IsValid)
            {
                db.Tbl_Registration.Add(tbl_Registration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("create");
        }


        [HttpPost]
        public ActionResult Login(Tbl_Registration objEntity)
        {
            if (ModelState.IsValid)
            {
                using (DB_WebDcrEntities db = new DB_WebDcrEntities())
                {
                    var obj = db.Tbl_Registration.Where(a => a.Email.Equals(objEntity.Email) && a.Password.Equals(objEntity.Password)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.User_Id.ToString();
                        Session["UserName"] = obj.First_Name.ToString()+ " "+obj.Last_Name.ToString();
                        return RedirectToAction("Create", "DCR_Master");
                    }
                }
            }  
            return RedirectToAction("Create");
        }

        // GET: Registration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Registration tbl_Registration = db.Tbl_Registration.Find(id);
            if (tbl_Registration == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Registration);
        }

        // POST: Registration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "User_Id,First_Name,Last_Name,Pan_Number,Company_Name,Mobile,Email,Password")] Tbl_Registration tbl_Registration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Registration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Registration);
        }

        // GET: Registration/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Registration tbl_Registration = db.Tbl_Registration.Find(id);
            if (tbl_Registration == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Registration);
        }

        // POST: Registration/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_Registration tbl_Registration = db.Tbl_Registration.Find(id);
            db.Tbl_Registration.Remove(tbl_Registration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public int generateID()
        {
            Random r = new Random();
            var x = r.Next(0, 1000000);
            return Convert.ToInt32(x.ToString("000000"));
        }
    }
}
